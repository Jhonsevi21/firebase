const registrar = document.getElementById('registar-r');

registrar.addEventListener('click', () => {
    const email = document.getElementById('email-r').value;
    const password = document.getElementById('password-r').value;

    firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function() {
            const alerta = document.getElementById('alert-message');
            alerta.innerHTML = `
            <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Registro Satisfactorio!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            `;
            setTimeout(function() {
                document.querySelector('.alert').remove();
            }, 2000);
            document.getElementById('email-r').value = '';
            document.getElementById('password-r').value = '';

            //Eliminando elementos de DOM despues del registro:
            document.querySelector('#registar-r').remove();
            //FIN

            //Añadiendo boton cerrar sesion, ya que al crear un nuevo
            //registro se inicia sesion por defecto:

            //Fin

            //Funcion que envia correo de verificacion:
            verificar();
            //Fin    
        })
        .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // Validando:
            if (errorCode == 'auth/invalid-email' && document.getElementById('email-r').value != '') {
                // validando formulario de registro:
                const alerta = document.getElementById('alert-message');
                alerta.innerHTML = `
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Formato de Correo Invalido!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            `;
                setTimeout(function() {
                    document.querySelector('.alert').remove();
                }, 3000);
                //Fin
            } else if (errorCode == 'auth/weak-password' && document.getElementById('password-r').value != '') {
                // validando formulario de registro:
                const alerta = document.getElementById('alert-message');
                alerta.innerHTML = `
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Contraseña muy corta! Minimo 6 caracteres</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            `;
                setTimeout(function() {
                    document.querySelector('.alert').remove();
                }, 3000);
                //Fin
            } else if (document.getElementById('email-r').value == '' || document.getElementById('password-r').value == '') {
                // validando formulario de registro:
                const alerta = document.getElementById('alert-message');
                alerta.innerHTML = `
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Llene todos los campos!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            `;
                setTimeout(function() {
                    document.querySelector('.alert').remove();
                }, 3000);
                //Fin
            }
        });

});

const ingresar = document.getElementById('ingresar');

ingresar.addEventListener('click', () => {

    const email2 = document.getElementById('email').value;
    const password2 = document.getElementById('password').value;
    firebase.auth().signInWithEmailAndPassword(email2, password2)
        .then(function() {


        })
        .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            const alerta = document.getElementById('verified');
            alerta.innerHTML = `
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Correo o Contraseña Invalido</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            `;
            setTimeout(function() {
                document.querySelector('.alert').remove();
            }, 3000);
        });
    document.getElementById('email').value = '';
    document.getElementById('password').value = '';
});

function observador() {

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            // User is signed in.
            console.log('sesion iniciada');
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;
            document.querySelector('#email').remove();
            document.querySelector('#password').remove();
            document.querySelector('#ingresar').remove();
            document.querySelector('#registrar').remove();
            const sesion = document.getElementById('sesion-iniciada');
            sesion.innerHTML =
                `
                <form>
            <button class="btn btn-danger my-2 my-sm-0" type="submit" id="cerrar-sesion" onclick="cerrar()">Cerrar Sesion</button>
            </form>
        `;
            contenido(emailVerified, email);
        } else {
            console.log('sesion no iniciada');
            // User is signed out.
            // ...
        }
    });
}
observador();

function contenido(emailVerified, email) {
    if (emailVerified) {
        const contenido = document.getElementById('contenido');
        contenido.innerHTML = `
    <h1>Bienvenido! ${email} </h1>
    `;
    } else {
        const verificado = document.getElementById('verified');
        verificado.innerHTML = `
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Verifique su Cuenta a traves de su correo!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            `;
    }



}

function cerrar() {
    firebase.auth().signOut()
        .then(function() {
            console.log('saliendose...');
        })
        .catch(function(error) {
            console.log(error);
        });
}

function verificar() {
    var user = firebase.auth().currentUser;

    user.sendEmailVerification().then(function() {
        // Email sent.
        console.log('enviando correo');
    }).catch(function(error) {
        // An error happened.
        console.log(error);
    });
}